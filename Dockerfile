# FastDFS
#
# VERSION 4.0.8

FROM debian:wheezy
MAINTAINER season summer summer@season

RUN apt-get update && apt-get install -y gcc gcc-multilib libc6-dev-i386 make nano htop --no-install-recommends 
RUN rm -rf /var/lib/apt/lists/*

#copy files
COPY  FastDFS_v4.08 /FastDFS_v4.08
COPY  libevent-2.0.14-stable /libevent-2.0.14
COPY  zlib-1.2.8 /zlib-1.2.8
COPY  fastdfs-nginx-module /fastdfs-nginx-module
COPY  nginx-1.8.0 /nginx-1.8.0
COPY docker-entrypoint.sh /entrypoint.sh

# build libevent
WORKDIR /libevent-2.0.14
RUN  ./configure --prefix=/usr/local/libevent-2.0.14 && make && make install  && make clean

RUN echo '/usr/local/libevent-2.0.14/include' >> /etc/ld.so.conf
RUN echo '/usr/local/libevent-2.0.14/lib' >> /etc/ld.so.conf
RUN ldconfig

#build fastdfs
WORKDIR /FastDFS_v4.08
RUN ./make.sh C_INCLUDE_PATH=/usr/local/libevent-2.0.14/include LIBRARY_PATH=/usr/local/libevent-2.0.14/lib && ./make.sh install && ./make.sh clean

#build nginx
WORKDIR /nginx-1.8.0
RUN ./configure --user=root --group=root --prefix=/etc/nginx --with-http_stub_status_module --with-zlib=/zlib-1.2.8 --without-http_rewrite_module --add-module=/fastdfs-nginx-module/src
RUN make
RUN make install
RUN make clean
RUN ln -sf /etc/nginx/sbin/nginx /sbin/nginx

RUN mkdir /fastdfs
RUN mkdir /fastdfs/tracker
RUN mkdir /fastdfs/store_path
RUN mkdir /fastdfs/client
#RUN mkdir /fastdfs/tracker/logs
RUN mkdir /fastdfs/storage
RUN mkdir /fdfs_conf
RUN cp /FastDFS_v4.08/conf/* /fdfs_conf
RUN cp /fastdfs-nginx-module/src/mod_fastdfs.conf /fdfs_conf

# forward request and error logs to docker log collector
#RUN ln -sf /dev/stdout /fastdfs/tracker/logs/trackerd.log

WORKDIR /

RUN chmod a+x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]